package vieon.training

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import vieon.training.databinding.FragmentHomeUserBinding
import vieon.training.ui.BriefcaseFragment
import vieon.training.ui.HeartFragment
import vieon.training.ui.HomeFragment
import vieon.training.ui.UserFragment


class HomeUserFragment : Fragment() {

    private var _binding: FragmentHomeUserBinding? = null

    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeUserBinding.inflate(inflater, container, false)
        val view = binding.root


        binding.navigation.setItemSelected(R.id.ic_home)
        replaceFragment(HomeFragment())

        binding.navigation.setOnItemSelectedListener { id ->
            when (id) {
                R.id.ic_home -> replaceFragment(HomeFragment())
                R.id.ic_heart -> replaceFragment(HeartFragment())
                R.id.ic_briefcase -> replaceFragment(BriefcaseFragment())
                else -> replaceFragment(UserFragment())

            }


        }

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun replaceFragment(fragment: Fragment) {
        val fragmentManger = activity?.supportFragmentManager
        val fragmentTransaction = fragmentManger?.beginTransaction()
        fragmentTransaction?.replace(R.id.frame_layout, fragment)
        fragmentTransaction?.commit()

    }


}
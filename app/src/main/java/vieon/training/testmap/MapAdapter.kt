package vieon.training.testmap

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import vieon.training.databinding.MapViewHolderBinding

class MapAdapter(private val list: HashMap<String, String>) :
    RecyclerView.Adapter<MapAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: MapViewHolderBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            MapViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            val keyArray = list.keys.toList()
            val key = keyArray[position]
            val value = list[key]
            binding.tvKey.text = key
            binding.tvValue.text = value


        }
    }
}
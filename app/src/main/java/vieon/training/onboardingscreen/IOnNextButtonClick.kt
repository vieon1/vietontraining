package vieon.training.onboardingscreen

interface IOnNextButtonClick {
    fun onNextButtonClick(position: Int)
}
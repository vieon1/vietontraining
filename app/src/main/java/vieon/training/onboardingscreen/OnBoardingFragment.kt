package vieon.training.onboardingscreen

import OnBoardingAdapter
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import vieon.training.R
import vieon.training.databinding.FragmentOnBoardingBinding
import vieon.training.model.OnBoardingModel


class OnBoardingFragment : Fragment(), IOnNextButtonClick {
    private var _binding: FragmentOnBoardingBinding? = null
    private val binding get() = _binding!!

    private lateinit var list: List<OnBoardingModel>

    companion object {
        const val ON_BOARDING = "OnBoarding"
        const val FINISHED = "Finished"

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOnBoardingBinding.inflate(inflater, container, false)
        val view = binding.root

        createData()

        val adapter = OnBoardingAdapter(this , list)
        binding.viewPager.adapter = adapter
        binding.indicator.attachTo(binding.viewPager)

        return view
    }

    private fun createData() {
        list = arrayListOf(
            OnBoardingModel(
                R.drawable.on_boarding_1,
                R.string.book_a_flight,
                R.string.on_boarding_1

            ),
            OnBoardingModel(
                R.drawable.on_boarding_2,
                R.string.find_a_hotel_room,
                R.string.on_boarding_2

            ),
            OnBoardingModel(
                R.drawable.on_boarding_3,
                R.string.enjoy_your_trip,
                R.string.on_boarding_3
            ),

            )
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun onBoardingIsFinished() {

        val sharedPreferences =
            requireActivity().getSharedPreferences(ON_BOARDING, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(FINISHED, true)
        editor.apply()
    }

    override fun onNextButtonClick(position: Int) {
        if (position < (binding.viewPager.adapter?.itemCount ?: 0) - 1) {
            binding.viewPager.setCurrentItem(position + 1, true)

        } else {
            onBoardingIsFinished()
            findNavController().navigate(R.id.action_onBoardingFragment_to_homeUserFragment)
        }
    }


}
package vieon.training.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import vieon.training.databinding.FragmentUserBinding
import vieon.training.testmap.MapAdapter


class UserFragment : Fragment() {

    private var _binding: FragmentUserBinding? = null
    private val binding get() = _binding!!


    enum class STATE {
        DELETE, UPDATE, INSERT
    }

    private lateinit var state: STATE
    private lateinit var list: HashMap<String, String>
    private lateinit var key: String
    private var value: Int? = null
    private lateinit var adapter: MapAdapter

    init {
        state = STATE.INSERT
        list = hashMapOf(
            "ABC" to "112",
            "B" to "2",
            "C" to "3"
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        adapter = MapAdapter(list)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        binding.recyclerView.adapter = adapter


        binding.btnInsert.setOnClickListener {
            state = STATE.INSERT
            binding.edtValue.visibility = View.VISIBLE
            binding.tvState.text = STATE.INSERT.toString()
        }
        binding.btnDelete.setOnClickListener {
            state = STATE.DELETE
            binding.edtValue.visibility = View.INVISIBLE
            binding.tvState.text = STATE.DELETE.toString()
        }
        binding.btnUpdate.setOnClickListener {
            state = STATE.UPDATE
            binding.edtValue.visibility = View.VISIBLE
            binding.tvState.text = STATE.UPDATE.toString()
        }

        binding.btnSubmit.setOnClickListener {
            if (checkData()) {
                when (state) {
                    STATE.INSERT -> insertElement()
                    STATE.DELETE -> deleteElement()
                    STATE.UPDATE -> updateElement()


                }
                binding.edtKey.setText("")
                binding.edtValue.setText("")
            }


        }


    }


    private fun checkData(): Boolean {
        if (binding.edtKey.text.isNullOrEmpty()) {
            Toast.makeText(context, "Key is empty!", Toast.LENGTH_SHORT).show()
            return false

        }
        if (binding.edtValue.text.isNullOrEmpty() && state != STATE.DELETE) {
            Toast.makeText(context, "Value is empty!", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }

    private fun insertElement() {
        val key = binding.edtKey.text.toString()
        val value = binding.edtValue.text.toString()
        if (!list.containsKey(key)) {
            list[key] = value
            val keyArray = list.keys.toList()
            adapter.notifyItemInserted(keyArray.indexOf(key))
            Toast.makeText(context, "Insert successfully", Toast.LENGTH_SHORT).show()

        } else Toast.makeText(context, "Element exists", Toast.LENGTH_SHORT).show()


    }

    private fun deleteElement() {
        val key = binding.edtKey.text.toString()
        if (list.containsKey(key)) {
            val keyArray = list.keys.toList()
            list.remove(key)
            adapter.notifyItemRemoved(keyArray.indexOf(key))
            Toast.makeText(context, "Delete successfully", Toast.LENGTH_SHORT).show()

        } else Toast.makeText(context, "Element does not exists", Toast.LENGTH_SHORT).show()
    }

    private fun updateElement() {
        val key = binding.edtKey.text.toString()
        val value = binding.edtValue.text.toString()
        if (list.containsKey(key)) {
            list[key] = value
            val keyArray = list.keys.toList()
            adapter.notifyItemChanged(keyArray.indexOf(key))
            Toast.makeText(context, "Update successfully", Toast.LENGTH_SHORT).show()

        } else Toast.makeText(context, "Element does not exists", Toast.LENGTH_SHORT).show()

    }


}
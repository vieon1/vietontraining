package vieon.training

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import vieon.training.onboardingscreen.OnBoardingFragment


class SplashFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if (onBoardingIsFinished()) {
            findNavController().navigate(R.id.action_splashFragment_to_homeUserFragment)
        } else {
            findNavController().navigate(R.id.action_splashFragment_to_onBoardingFragment)
        }
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    private fun onBoardingIsFinished(): Boolean {
        val sharePreferences = requireActivity().getSharedPreferences(
            OnBoardingFragment.ON_BOARDING,
            Context.MODE_PRIVATE
        )
        return sharePreferences.getBoolean(
            OnBoardingFragment.FINISHED,
            false
        )

    }
}
package vieon.training.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class OnBoardingModel(
    @DrawableRes val image: Int,
    @StringRes val textTitle: Int,
    @StringRes val textDescription: Int,

    )

